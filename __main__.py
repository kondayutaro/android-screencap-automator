import os
import subprocess
from datetime import datetime
from time import sleep


class Utility():
    def __init__(self) -> None:
        pass

    def get_devices(self) -> list:
        device_list = subprocess.check_output(('adb', 'devices')).decode('utf-8')
        device_list = device_list.replace('\t', '\n').split('\n')
        device_list.remove('List of devices attached')
        while 'device' in device_list: device_list.remove('device')
        while '' in device_list: device_list.remove('')
        return device_list

    def format_property(self, raw_string: bytes) -> str:
        raw_string = raw_string.decode('utf-8')
        raw_string = raw_string.replace("b'", "")
        raw_string = raw_string.replace('\n', ' ').replace('\r', '')
        raw_string = raw_string.replace(' ', '')
        return raw_string

    def folder_exists(self, folder_name) -> None:
        if not os.path.isdir(folder_name):
            os.makedirs(folder_name)


# Temporaily store screencaps in phone storage
output_dir = '/sdcard'
# Specify where to store the screencaps
save_dir = '/Volumes/SanDisk External SSD/Android Screencaps'
# Time interval to take screencap (seconds)
interval = 30

utility = Utility()
# Check if the specified directory already exists
utility.folder_exists(save_dir)

while 9 < datetime.now().time().hour < 21:
    for serial_number in utility.get_devices():
        # If the device is inactive, skip screencap
        display = utility.format_property(subprocess.check_output(f"adb -s {serial_number} shell dumpsys power | grep 'mHoldingDisplaySuspendBlocker=' | grep -o -E 'false|true'", shell=True))
        if display == 'false':
            break

        # Works only up until Android 9. Outputs "Android Q or Later" for Android 10+
        application_name = utility.format_property(subprocess.check_output(f"adb -s {serial_number} shell dumpsys activity recents | grep 'Recent #0' | cut -d= -f2 | sed 's| .*||' | cut -d '/' -f1", shell=True))
        if application_name == "true":
            application_name = "Android Q or Later"
        
        product_name = utility.format_property(subprocess.check_output(('adb', '-s', serial_number, 'shell', 'getprop', 'ro.product.model')))
        print(product_name)
        os_version = utility.format_property(subprocess.check_output(('adb', '-s', serial_number, 'shell', 'getprop', 'ro.build.version.release')))
        print(os_version)

        file_name = datetime.now().strftime("%Y%m%d_%H%M%S")
        print(file_name)
        temporary_image_location = f'{output_dir}/{file_name}.png'
        save_dir_for_device = f"{save_dir}/{application_name}/{product_name}[{os_version}]"
        print(save_dir_for_device)
        utility.folder_exists(save_dir_for_device)
        subprocess.call(('adb', '-s', serial_number, 'shell', 'screencap', temporary_image_location))
        subprocess.call(('adb', '-s', serial_number, 'pull', temporary_image_location, save_dir_for_device))
    sleep(interval)

